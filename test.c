#include <stdio.h>

int main()
{
    int ar[3];
    ar[0] = 5;
    ar[1] = 6;
    ar[12] = 12;
    ar[21] = 21;

    int *p;
    p = &ar[0];

    int width = sizeof(ar[0]);

    printf("%d\n", width);
    printf("%d", *p);
    printf("%d", *(p+width));
    printf("%d", *(p+width));
}
